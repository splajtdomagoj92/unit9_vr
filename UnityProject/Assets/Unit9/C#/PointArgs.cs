﻿using System;

namespace Unit9
{
    /// <summary>
    /// Used when player shoot an enemy.
    /// </summary>
    public class PointArgs : EventArgs
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        /// <summary>
        /// How many points did enemy give
        /// </summary>
        public int Points { get; private set; }

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        public PointArgs(int points)
        {
            Points = points;
        }

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
