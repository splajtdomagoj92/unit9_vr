﻿using UnityEngine;

namespace Unit9
{
    public class PlayerController : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [Tooltip("How many points did player get so far")]
        [SerializeField]
        private PointsComponent _pointsComponent;

        [SerializeField]
        private WeaponComponent _weaponComponent;

        [SerializeField]
        private HealthComponent _healthComponent;
        public HealthComponent HealthComponent => _healthComponent;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        private void Awake()
        {
            // Just to initialize with text
            _pointsComponent.Add(0);

            // Subscribe to know when weapon shot an enemy
            _weaponComponent.OnEnemyHit += OnEnemyHit;
        }

        public void OnEnemyHit(object sender, PointArgs args)
        {
            // Add points
            _pointsComponent.Add(args.Points);
        }

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
