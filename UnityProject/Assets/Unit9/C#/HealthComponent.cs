﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Unit9
{
    public class HealthComponent : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private float _healthStart;

        private float _healthCurrent;

        [Tooltip("Fills up and down")]
        [SerializeField]
        private Image _bar;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        private void Start()
        {
            _healthCurrent = _healthStart;
            UpdateHealthBar();
        }

        /// <summary>
        /// Reduce health by amount
        /// </summary>
        public void ReduceHealth(int amount)
        {
            _healthCurrent -= amount;
            UpdateHealthBar();

            if(_healthCurrent <= 0) OnHealthZero?.Invoke(null, EventArgs.Empty);
        }

        private void UpdateHealthBar()
        {
            _bar.fillAmount = _healthCurrent / _healthStart;
        }

        /// <summary>
        /// Invoked once health reached zero
        /// </summary>
        public event EventHandler<EventArgs> OnHealthZero;

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
