﻿using UnityEngine;
using UnityEngine.UI;

namespace Unit9
{
    public class PointsComponent : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private Text _textPoints;

        [SerializeField]
        private string _pointsPrefix = "Points: ";

        /// <summary>
        /// How many points player collected so far
        /// </summary>
        private int _points;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        public void Add(int points)
        {
            _points += points;
            _textPoints.text = _pointsPrefix + _points;
        }

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
