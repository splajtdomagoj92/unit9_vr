﻿using UnityEngine;

namespace Unit9
{
    [CreateAssetMenu(fileName = "Enemy_", menuName = "Unit9/Enemies/Enemy")]
    public class EnemySO : ScriptableObject
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private float _speed;
        /// <summary>
        /// Movement speed
        /// </summary>
        public float Speed => _speed;

        [SerializeField]
        private int _points;
        /// <summary>
        /// How many points does player get for shooting this enemy
        /// </summary>
        public int Points => _points;

        [SerializeField]
        private int _damage;
        /// <summary>
        /// How much damage is dealt to player if it hits the player
        /// </summary>
        public int Damage => _damage;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods


        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
