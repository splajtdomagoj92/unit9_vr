﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace Unit9
{
    public class SpawnController : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private List<EnemyController> _prefabs = new List<EnemyController>();

        private List<EnemyController> _instantiated = new List<EnemyController>();

        [Tooltip("Contains instantiated enemies")]
        [SerializeField]
        private Transform _container;

        [Tooltip("Distance from player at which enemy is instantiated")]
        [SerializeField]
        private float _spawnDistance;

        [Tooltip("Distance from player at which enemy is instantiated")]
        [SerializeField]
        private float _spawnDistanceSide;

        [SerializeField]
        private float _enemySpawnTime;

        private Coroutine _coroutineEnemy;

        /// <summary>
        /// Prevents creating excesive garbage
        /// </summary>
        private WaitForSeconds _waitTime;

        [SerializeField]
        private PlayerController _player;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        private void Start()
        {
            _waitTime = new WaitForSeconds(_enemySpawnTime);
            _coroutineEnemy = StartCoroutine(CoroutineSpawnEnemy());

            _player.HealthComponent.OnHealthZero += OnHealthZero;
        }

        private IEnumerator CoroutineSpawnEnemy()
        {
            while (true)
            {
                yield return _waitTime;

                CreateEnemy();
            }
        }

        private void CreateEnemy()
        {
            EnemyController _controller = _instantiated.FirstOrDefault(x => !x.gameObject.activeSelf);
            if(_controller == null)
            {
                int _count = _prefabs.Count;
                int _rnd = UnityEngine.Random.Range(0, _count);

                GameObject _go = Instantiate(_prefabs[_rnd].gameObject, _container);
                _controller = _go.GetComponent<EnemyController>();

                _controller.SetTarget(_player.transform);
                _instantiated.Add(_controller);
            }

            _controller
                .SetPosition(GetSpawnPosition())
                .Activate()
            ;
        }

        private Vector3 GetSpawnPosition()
        {
            Vector3 _side = (int)UnityEngine.Random.Range(0, 2) == 0 ? -_player.transform.right : _player.transform.right;

            // Spawn in front of player at a side
            return _player.transform.position + _player.transform.forward * _spawnDistance + _spawnDistanceSide * _side;
        }

        private void OnHealthZero(object sender, EventArgs args)
        {
            StopCoroutine(_coroutineEnemy);
            int count = _instantiated.Count;
            for (int i = 0; i < count; i++)
            {
                _instantiated[i].gameObject.SetActive(false);
            }
        }

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
