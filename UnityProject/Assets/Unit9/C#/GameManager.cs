﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Unit9
{
    /// <summary>
    /// Activates Game Over UI once player is destroyed
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private PlayerController _playerController;

        [SerializeField]
        private GameObject _buttonShoot;

        [SerializeField]
        private GameObject _gameOverUI;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        private void Awake()
        {
            _playerController.HealthComponent.OnHealthZero += OnHealthZero;
        }

        /// <summary>
        /// Player destroyed. Setup game over UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnHealthZero(object sender, EventArgs args)
        {
            _buttonShoot.SetActive(false);
            _gameOverUI.SetActive(true);
        }

        /// <summary>
        /// Restart current level
        /// </summary>
        public void ReloadLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
