﻿using UnityEngine;
using System;

namespace Unit9
{
    public class EnemyController : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private EnemySO _so;

        /// <summary>
        /// Player target
        /// </summary>
        private Transform _target;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods Setup

        public EnemyController SetTarget(Transform target)
        {
            _target = target;
            return this;
        }

        public EnemyController SetPosition(Vector3 position)
        {
            transform.position = position;
            return this;
        }

        public EnemyController Activate()
        {
            gameObject.SetActive(true);
            return this;
        }

        #endregion Methods Setup

        ////////////////////////////////////////////////////////////////////////

        #region Methods Attack Player

        private void Update()
        {
            if(_so && _target)
            {
                transform.position = Vector3.Lerp(transform.position, _target.position, Time.deltaTime * _so.Speed);
            }
        }

        /// <summary>
        /// Deal damage to player if collided with them
        /// </summary>
        private void OnTriggerEnter(Collider col)
        {
            PlayerController player = col.GetComponent<PlayerController>();
            if(player)
            {
                player.HealthComponent.ReduceHealth(_so.Damage);
                gameObject.SetActive(false);
            }
        }

        #endregion Methods Attack Player

        ////////////////////////////////////////////////////////////////////////

        #region Methods Take Damage

        /// <summary>
        /// Destroys enemy
        /// </summary>
        public int TakeDamage()
        {
            gameObject.SetActive(false);
            OnDestroyed?.Invoke(null, EventArgs.Empty);

            if (_so) return _so.Points;

            else
            {
                #if UNITY_EDITOR
                Debug.LogError("Missing EnemySO reference!");
                #endif

                return 0;
            }
        }

        /// <summary>
        /// Invoked once player destroyed this enemyy
        /// </summary>
        public event EventHandler<EventArgs> OnDestroyed;

        #endregion Methods Take Damage

        ////////////////////////////////////////////////////////////////////////
    }
}
