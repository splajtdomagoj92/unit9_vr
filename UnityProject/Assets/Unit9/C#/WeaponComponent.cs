﻿using UnityEngine;
using System;

namespace Unit9
{
    public class WeaponComponent : MonoBehaviour
    {
        ////////////////////////////////////////////////////////////////////////

        #region Vars

        [SerializeField]
        private ParticleSystem _fx;

        [Tooltip("How much it takes before player can shoot again")]
        [SerializeField]
        private float _cooldownTime;

        private float _time = 1;

        #endregion Vars

        ////////////////////////////////////////////////////////////////////////

        #region Methods

        private void Update()
        {
            Cooldown();
        }

        private void Cooldown()
        {    
            if(_time < 1)
            {
                _time += Time.deltaTime / _cooldownTime;
            }
        }

        /// <summary>
        /// Shoot and return amount of points collected depending on hit target
        /// </summary>
        /// <returns></returns>
        public void Shoot()
        {
            if(_time >= 1)
            {
                _time = 0;

                if (_fx) _fx.Play();

                RaycastHit raycast;
                // Shoot forward from this position
                if (Physics.Raycast(transform.position, transform.forward, out raycast, Mathf.Infinity))
                {
                    // Check if hit enemy
                    EnemyController enemy = raycast.transform.GetComponent<EnemyController>();
                    if (enemy)
                    {
                        OnEnemyHit?.Invoke(null, new PointArgs(enemy.TakeDamage()));
                    }

                    else Debug.Log("A");
                }
            }
        }

        /// <summary>
        /// Invoked once this weapon shot an enemy
        /// </summary>
        public event EventHandler<PointArgs> OnEnemyHit;

        #endregion Methods

        ////////////////////////////////////////////////////////////////////////
    }
}
